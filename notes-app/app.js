const chalk = require("chalk");
const { argv } = require("yargs");
const yargs = require("yargs");
const notes = require("./notes.js");

yargs.version("1.1.0");
yargs.command({
  command: "add",
  describe: "add a new note",
  builder: {
    title: {
      describe: "Note title",
      demandOPtion: true,
      type: "string",
    },
    body: {
      describe: "Note body",
      demandOPtion: true,
      type: "string",
    },
  },
  handler(argv) {
    notes.addNote(argv.title, argv.body);
    // console.log("Title", argv.title);
    // console.log("body", argv.body);
  },
});

yargs.command({
  command: "remove",
  describe: "Remove a note",
  builder: {
    title: {
      describe: "note title",
      demandOption: true,
      type: "string",
    },
  },
  handler(argv) {
    notes.removeNote(argv.title);
  },
});
yargs.command({
  command: "list",
  describe: "List a note",
  handler() {
    notes.listNotes();
  },
});
yargs.command({
  command: "read",
  describe: "read a note",
  builder: {
    title: {
      describe: "Note title",
      demandOPtion: true,
      type: "string",
    },
  },
  handler(argv) {
    notes.readNote(argv.title);
  },
});

yargs.parse();

// console.log(yargs.argv);

// const command = process.argv[2];

// console.log(process.argv);
// if (command === "add") {
//   console.log("Adding note");
// } else if (command === "remove") {
//   console.log("Removing note");
// }

// console.log(chalk.green("Success"));
// console.log(chalk.bold("Success"));
// console.log(chalk.inverse("Success"));

// const validator = require("validator");

// const getNotes = require("./notes.js");
// const msg = getNotes();
// console.log(msg);
// console.log(validator.isEmail("jahera@gmail.com"));

// const add = require("./utils.js");
// const sum = add(5, -2);
// console.log(sum);
