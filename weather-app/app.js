const geocode = require("./utils/geocode.js");
const forecast = require("./utils/forecast.js");

///destructuring

const address = process.argv[2];
if (!address) {
  console.log("please provided address");
} else {
  geocode(address, (error, { latitude, longitude, location } = {}) => {
    if (error) {
      return console.log(error);
    }
    forecast(latitude, longitude, (error, forecastData) => {
      if (error) {
        return console.log(error);
      }
      console.log(location);
      console.log(forecastData);
    });
  });
}
console.log(process.argv);

// const address = process.argv[2];
// if (!address) {
//   console.log("please provided address");
// } else {
//   geocode(address, (error, data) => {
//     if (error) {
//       return console.log(error);
//     }
//     forecast(data.latitude, data.longitude, (error, forecastData) => {
//       if (error) {
//         return console.log(error);
//       }
//       console.log(data.location);
//       console.log(forecastData);
//     });
//   });
// }
// console.log(process.argv);

// const url =
//   "http://api.weatherstack.com/current?access_key=0aed906fa7c1f59246137e12ebcecbd7&query=37.8267,-122.4233&units=f";
// request({ url: url, json: true }, (error, response) => {
//   if (error) {
//     console.log("Unable to connect to weather service");
//   } else if (response.body.error) {
//     console.log("Unable to find location");
//   } else {
//     console.log(
//       response.body.current.weather_descriptions[0] +
//         ": It is currently " +
//         response.body.current.temperature +
//         " degrees out.It feels like " +
//         response.body.current.feelslike +
//         " degrees out. There is a " +
//         response.body.current.precip +
//         "% chance of rain"
//     );
//   }

//   console.log(response.body.current);
//   const current_temp = response.body.current.temperature;
//   console.log(
//     response.body.current.weather_descriptions[0] +
//       ": It is currently " +
//       response.body.current.temperature +
//       " degrees out.It feels like " +
//       response.body.current.feelslike +
//       " degrees out. There is a " +
//       response.body.current.precip +
//       "% chance of rain"
//   );
// });
