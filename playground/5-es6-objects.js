const name = "jahera";
const userAge = 27;
const user = {
  name,
  age: userAge,
  location: "India",
};
console.log(user);

///object destructuring

const product = {
  label: "Red notebook",
  price: 3,
  stock: 201,
  saleprice: 201,
  saleprice: undefined,
};
// const label=product.label
// const stock=product.stock

// const { label: productLabel, stock, rating = 5 } = product;
// console.log(productLabel);
// console.log(stock);
// console.log(rating);

const transaction = (type, { label, stock = 0 } = {}) => {
  console.log(type, label, stock);
};

// transaction("order", product);
transaction("order", product);
